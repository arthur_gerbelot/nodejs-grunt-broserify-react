module.exports = {
  HOST: 'http://127.0.0.1',
  PORT: 3500,

  SESSION: {
    KEY: 'NodeBaseProject',
    TIMEOUT: 3600000, // 1h
  }
}