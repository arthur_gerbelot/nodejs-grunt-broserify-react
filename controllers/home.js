'use strict';

var React = require('react');
require('node-jsx').install({extension: '.jsx', harmony: true});

var MainApp = require('../assets/components/app.jsx');

module.exports = {
  index: function(req, res, next) {

    var react_data = {
      items: [
        {name: 'Arthur'},
        {name: 'Paul'},
        {name: 'Laura'}
      ]
    };

    var App = React.createElement(MainApp, react_data);
    var react_app = React.renderToString(App);

    res.render('home/index', {
      react_app: react_app,
      react_data: react_data
    });
  }
};