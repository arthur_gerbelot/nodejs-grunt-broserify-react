var _     = require('lodash');
var $     = require('jquery');
var React = require('react');

var MainApp = require('../components/app.jsx');

global.MainController = new function() {
  var self = this;

  this.react_data = {}; // This will be updated by Controller react data on /views/layout.jade

  this.init = function() {
    // Re Render to bind events
    self.reRender();
  };

  // ReRender React components
  this.reRender = function() {
    var App = React.createElement(MainApp, self.react_data);
    React.render(
      App,
      document.getElementById('react-app')
    );
  }
};