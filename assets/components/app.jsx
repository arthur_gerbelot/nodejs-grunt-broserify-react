var _      = require('lodash');
var $      = require('jquery');
var React  = require('react');

var MainApp = React.createClass({
  getInitialState: function() {
    return {
      new_name: '',
      items: this.props.items || []
    };
  },

  changeHandler: function(e) {
    this.setState({new_name: e.target.value})
  },

  clickHandler: function(e) {
    // Trigger event
    var items = this.state.items;
    console.log(this.state);
    items.push({name: this.state.new_name});

    // Reset state
    this.setState({
      new_name: '',
      items: items
    });
  },

  render: function() {
    var self = this;

    var items = _.map(this.state.items, function(item, i) {
      return (
        <li key={'item-' + i}>
          {item.name}
        </li>
      );
    });

    return (
      <div>
        <h3>
          <i className='fa fa-thumbs-up'/>
          It s crazy here !
        </h3>
        <hr/>
        <p>List of item : </p>
        <ul>{items}</ul>
        <hr/>
        <p>Add new :</p>
        <input type='text' name='new_name' value={self.state.new_name} onChange={this.changeHandler}/>
        <a onClick={self.clickHandler}>[ADD]</a>
      </div>
    );
  }
});

module.exports = MainApp;