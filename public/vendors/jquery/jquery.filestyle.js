/*
 * Style File - jQuery plugin for styling file input elements
 *
 * Copyright (c) 2007-2008 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Based on work by Shaun Inman
 *   http://www.shauninman.com/archive/2007/09/10/styling_file_inputs_with_css_and_the_dom
 *
 * Revision: $Id: jquery.filestyle.js 303 2008-01-30 13:53:24Z tuupola $
 *
 */

(function($) {

    $.fn.filestyle = function(options) {

        /* TODO: This should not override CSS. */
        var settings = {
            width : 'auto',
            imageheight : 'auto',
            imagewidth : 'auto',
            text: 'Browse'
        };

        if(options) {
            $.extend(settings, options);
        };

        return this.each(function() {

            var self = this;
            //Create Wrapper
            var wrapper = $("<div class='file-container'>")
                            .css({
                                "display": "inline-block",
                                "position": "relative",
                                'overflow': 'hidden'
                            });
            //Create input showing selected file
            var filename = $('<input class="file">').attr('disabled', 'disabled')
                             .addClass($(self).attr("class"))
                             .css({
                                 "display": "inline-block",
                                 "position": "relative",
                                 "zIndex": "3"
                             }).val($('input[type=hidden][name='+this.name+']').val());
            //Our new input should have the same width as the original file input
            if (settings.width == 'auto') {
                settings.width = parseInt($(this).width());
            }        

            //Add new elements to the DOM
            $(self).before(filename);
            $(self).wrap(wrapper);
            $(self).after('<span class="button">'+settings.text+'</span>');
            wrapper = $(self).parent();
            
            $('.button', wrapper).wrap('<span class="button-container"></span>');
            $('.button-container', wrapper).css({
                'position': 'absolute',
                'left': '0',
                'top'  : 0
            });

            //bugfix for width in Firefox
            $('.button', wrapper).width($('.button', wrapper).width());

            if (settings.imagewidth == 'auto') {
                settings.imagewidth = $('.button', wrapper).outerWidth(true);
            }

            if (settings.imageheight == 'auto') {
                settings.imageheight = $('.button', wrapper).outerHeight(true);
            }


            $(wrapper).css({
                width: settings.imagewidth,
                height: settings.imageheight
            });
            
            $(filename).css('width', settings.width-parseInt($(filename).css('paddingLeft'))-parseInt($(filename).css('paddingRight')-settings.imagewidth));

            $(self).css({
                        "position": "relative",
                        "height": settings.imageheight + "px",
                        "width": settings.width+ "px",
                        "display": "block",
                        "cursor": "pointer",
                        "opacity": "0",
                        "zIndex": "2"
                    });
            
            
            $(self).css("margin-left", settings.imagewidth - settings.width + "px");

            //add a class on input hovering
            $(this).mouseover(function() {$(this).parent().find('.button').addClass('hover')})
            .mouseout(function() {$(this).parent().find('.button').removeClass('hover')});
                
            //Add filepath to the fake input
            $(self).bind("change", function() {
                var path = $(self).val();
                if (path.indexOf('fakepath') != -1) {
                    if (path.indexOf('\\') != -1) {
                        path = path.split('\\').pop();
                    } else {
                        path = path.split('/').pop();
                    }
                }
                filename.val(path);
            });

        });


    };

})(jQuery);
