(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var _      = require('lodash');
var $      = require('jquery');
var React  = require('react');

var MainApp = React.createClass({displayName: "MainApp",
  getInitialState: function() {
    return {
      new_name: '',
      items: this.props.items || []
    };
  },

  changeHandler: function(e) {
    this.setState({new_name: e.target.value})
  },

  clickHandler: function(e) {
    // Trigger event
    var items = this.state.items;
    console.log(this.state);
    items.push({name: this.state.new_name});

    // Reset state
    this.setState({
      new_name: '',
      items: items
    });
  },

  render: function() {
    var self = this;

    var items = _.map(this.state.items, function(item, i) {
      return (
        React.createElement("li", {key: 'item-' + i}, 
          item.name
        )
      );
    });

    return (
      React.createElement("div", null, 
        React.createElement("h3", null, 
          React.createElement("i", {className: "fa fa-thumbs-up"}), 
          "It s crazy here !"
        ), 
        React.createElement("hr", null), 
        React.createElement("p", null, "List of item : "), 
        React.createElement("ul", null, items), 
        React.createElement("hr", null), 
        React.createElement("p", null, "Add new :"), 
        React.createElement("input", {type: "text", name: "new_name", value: self.state.new_name, onChange: this.changeHandler}), 
        React.createElement("a", {onClick: self.clickHandler}, "[ADD]")
      )
    );
  }
});

module.exports = MainApp;

},{"jquery":"jquery","lodash":"lodash","react":"react"}],2:[function(require,module,exports){
(function (global){
var _     = require('lodash');
var $     = require('jquery');
var React = require('react');

var MainApp = require('../components/app.jsx');

global.MainController = new function() {
  var self = this;

  this.react_data = {}; // This will be updated by Controller react data on /views/layout.jade

  this.init = function() {
    // Re Render to bind events
    self.reRender();
  };

  // ReRender React components
  this.reRender = function() {
    var App = React.createElement(MainApp, self.react_data);
    React.render(
      App,
      document.getElementById('react-app')
    );
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"../components/app.jsx":1,"jquery":"jquery","lodash":"lodash","react":"react"}]},{},[2]);
