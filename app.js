var http            = require('http');
var express         = require('express');
var jade            = require('jade');
var favicon         = require('serve-favicon');
var colors          = require('colors');
var session         = require('express-session');
var CookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');

var config          = require('./config/app');
var routes          = require('./routes');

var cookieParser    = CookieParser(config.SESSION.KEY)

// Start server
var app = module.exports = express();

// Configure app
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.engine('.html', jade.__express);

// Public directory
app.use(express.static(__dirname + '/public'));

// Handle Favicon request
app.use(favicon(__dirname + '/assets/img/favicon.ico'));

// Set session
app.use(cookieParser);
// Set bodyParser
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded()); // to support URL-encoded bodies

// Setup routes
routes.setup(app);

// Handling error 404
app.use(function(req, res) {
  console.log("Error 404. Nothing for " + req.method + " " + req.url);
  res.send("Hum.. 404 Here. Nothing for " + req.method + " " + req.url, 404);
});

// Handling error 500
app.use(function(err, req, res, next) {
  console.log("Error 500.", err.stack);
  res.send("Hum.. An 500 error are occured for " + req.method + " " + req.url, 500);
});

process.on('uncaughtException', function(err) {
  console.log("Error 500 throw here. Process crash.")
  console.log(err.stack);
});


// Start server
var server = app.listen(process.env.PORT || config.PORT, function() {
  var host = server.address().address
  var port = server.address().port

  for (var i=0; i < 1; i++) {
    console.log(" ")
  }
  console.log(" Server listen at ")
  console.log("    http://" + host + ":" + port)
  for (var i=0; i < 5; i++) {
    console.log(" ")
  }
});