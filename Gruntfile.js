'use strict';

var _   = require('underscore');

module.exports = function(grunt) {

  // load all grunt tasks automatically, but exclude grunt-cli because it is not a grunt task
  _( require('matchdep').filterAll('grunt-*') ).without('grunt-cli').forEach( grunt.loadNpmTasks );

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // clean our public files
    clean: {
      build: ['public'],
      js: ['public/js'],
      css: ['public/css'],
      img: ['public/img']
    },

    copy: {
      css: {
        expand: true,
        cwd: 'assets/css',
        src: '**',
        dest: 'public/css'
      },
      img : {
        expand: true,
        cwd: 'assets/img',
        src: '**',
        dest: 'public/img'
      },
      vendors : {
        expand: true,
        cwd: 'assets/vendors',
        src: '**',
        dest: 'public/vendors'
      },
    },

    concurrent: {
      dev: {
        tasks: ['nodemon', 'watch'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    nodemon: {
      dev: {
        script: 'app.js'
      }
    },

    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/scss',
          src: ['*.scss'],
          dest: 'assets/css',
          ext: '.css'
        }]
      }
    },

    browserify: {
      vendor: {
        src: [],
        dest: 'public/js/vendor-bundle.js',
        options: {
          require: ['backbone', 'react', 'jquery', 'lodash', 'moment', 'URIjs']
        }
      },
      main: {
        src: 'assets/js/main.js',
        dest: 'public/js/main-bundle.js',
        options: {
          external: ['backbone', 'react', 'jquery', 'lodash', 'moment', 'URIjs'],
          transform: ['reactify']
        }
      }
    },

    watch: {
      // recopy single js scripts
      scripts: {
        files: ['assets/js/*.js', 'assets/components/*.jsx', 'config/*.js', 'app.js'],
        tasks: ['clean:js', 'browserify']
      },
      scss: {
        files: ['assets/scss/*.scss'],
        tasks: ['clean:css', 'sass', 'copy:css']
      },
      img: {
        files: ['assets/img/*'],
        tasks: ['clean:img', 'copy:img']
      },
      options: {
        nospawn: true
      }
    }
  });


  grunt.registerTask('copy-assets', ['copy:css', 'copy:img', 'copy:vendors']);

  grunt.registerTask('build-common', ['clean:build', 'sass', 'copy-assets', 'browserify']);
  grunt.registerTask('build-dev', ['build-common']);

  grunt.registerTask('dev', ['build-dev', 'concurrent:dev']);

  grunt.registerTask('postinstall', [])
};
